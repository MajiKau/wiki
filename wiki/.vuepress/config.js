const proc = require('process');
const path = require('path')
const VideoHtml = require('markdown-it-html5-embed')

const headers = [
    ['link', { rel: "icon", type: "image/png", href: "/blrevive.png"}],
    ['link', { rel: "shortcut icon", href: "/blrevive.ico"}],
]

if(process.env.PREVIEW_ALLOW_REVIEW) {
    headers.push(
        ['script', {
            defer: null,
            'data-project-id': process.env.CI_MERGE_REQUEST_PROJECT_ID,
            'data-project-path': process.env.CI_MERGE_REQUEST_PROJECT_PATH,
            'data-merge-request-id': process.env.CI_MERGE_REQUEST_IID,
            'data-mr-url': 'https://gitlab.com',
            id: 'review-app-toolbar-script',
            src: 'https://gitlab.com/assets/webpack/visual_review_toolbar.js'
        }])
}


module.exports = {
    title: 'BLRevive',
    description: 'A wiki for BLRevive',
    base: proc.env.WIKI_BASE || '/wiki/',
    head: headers,
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { 
                text: 'Users',
                items: [
                    { text: 'Getting Started', link: '/guides/user/getting-started' },
                    { text: 'Contribute', link: '/guides/user/contribution'},
                    {
                        text: 'Launcher',
                        items: [
                            { text: 'GUI', link: '/guides/launcher/gui.md' },
                            { text: 'CLI', link: '/guides/launcher/cli.md' }
                        ]
                    },
                ]
            },
            {
                text: 'Hosters',
                items: [
                    { 
                        text: 'Game Server',
                        items: [
                            { text: 'Basics', link: '/guides/hosting/game-server/getting-started' },
                            { text: 'Parameters', link: '/guides/hosting/game-server/parameters.md' },
                            { text: 'VM / Docker', link: '/guides/hosting/game-server/vm-docker.md'}
                        ]
                    },
                    { 
                        text: 'Super Server',
                        items: [
                            { text: 'Basics', link: '/guides/hosting/super-server/getting-started'}
                        ]
                    },
                ]
            },
            {
                text: 'Developers',
                items: [
                    { text: 'Getting Started', link: '/dev/getting-started' },
                    { text: 'Contribution Guideline', link: '/dev/contribution.md'},
                    { 
                        text: 'Frontend',
                        items: [
                            { text: 'Launcher', link: '/dev/frontend/launcher/' },
                            { text: 'Website', link: '/dev/frontend/web/'}
                        ]
                    },
                    {
                        text: 'Backend',
                        items: [
                            { text: 'Super Server', link: '/dev/backend/super-server/'},
                            { text: 'ZCAPI', link: '/dev/backend/zcapi/' },
                        ]
                    },
                    {
                        text: 'Tools',
                        items: [
                            { text: 'SDK Generator', link: '/dev/tools/sdk-generator/'},
                            { text: 'SDK', link: '/dev/tools/sdk/'},
                            { text: 'Proxy', link: '/dev/tools/proxy/'},
                            { text: 'ServerUtil', link: '/dev/tools/server-util/' },
                        ]
                    },
                    {
                        text: 'Reverse Engineering',
                        items: [
                            { text: 'Getting Started', link: '/dev/re'}
                        ]
                    }
                ]
            }
        ],
        sidebar: 'auto',
        repo: 'https://gitlab.com/blrevive',
        repoLabel: 'GitLab',
        docsRepo: 'https://gitlab.com/blrevive/wiki',
        docsBranch: 'master',
        editLinks: true,
        editLinkText: 'Improve this page!',
    },
    configureWebpack: {
        resolve: {
            alias: {
                '@imgs': path.resolve(__dirname, '/../assets/images')
            }
        }
    },
    markdown: {
        plugins: {
            'markdown-it-html5-embed': {
                html5embed: { useLinkSyntax: true }
            }
        }
    }
}