# :video_game: Getting Started 

Welcome to the world of BLRevive, a place for all Blacklight refugees! This guide should help you installing and configuring Blacklight: Retribution to play on our Community Servers.

::: tip Discord
Make sure to join our [Discord] if you haven't allready for recent updates, discussions
and help about this project.
:::


## :package: Install Launcher

1. Download [Blacklight: Retribution v3.02](https://steamdb.info/app/209870/)
    ::: warning BL:R Client download links
    To avoid legal issues we decided to not provide download links for the client itself. However it's easy to find the links by searching for something like *Blacklight:Retribution v3.01 Client Download* !
    :::
1. Download the latest Launcher
    - [Windows](https://gitlab.com/blrevive/launcher/-/package_files/41812015/download)
    - [Linux](https://gitlab.com/blrevive/launcher/-/package_files/41811989/download)

## :wrench: Configure Launcher

The next step is to configure the launcher.

### :one: Add BL:R client

You need to provide at least one BL:R client (v3.02/v3.01) to the launcher to be able to play.

- open the launcher
- go to `Settings`
- drag & drop the `<BLR>/Binaries/Win32/FoxGame-win32-Shipping.exe` into the `Game Clients` grid <InlinePreview>![AddGameClientDragDrop](./AddBlrClient.mp4)</InlinePreview>
::: warning not available on Linux currently
Avalonia has currently no/limited support for drag & drop on linux which is why you need to this instead:

- click on `Add`  beside the `Game Clients` grid
- click on `Browser`  *or* enter path to `<BLR>/Binaries/Win32/FoxGame-win32-Shipping.exe`
- click `Save` to add the client
:::


### :two: Patch BL:R client

After adding the client you also need to apply our patches.

- goto `Settings`
- select unpatched client
- click on `Patch` <InlinePreview>![PatchBlrClient](./PatchBlrClient.mp4)</InlinePreview>

::: warning Bug: indicator stays red
There is a bug with the patch indicator (the red/green box) which shows still red (unpatched)
after a succesfull patch. After restarting the launcher it will update properly!
([BLRevive/Launcher #6](https://gitlab.com/blrevive/launcher/-/issues/6))
:::


## :arrow_forward: Join Game Server

You can either use our server browser which gives you easy access to running game servers or connect to a specific server by address.

### Community Servers

Our [Discord]() already provides a few servers which allow loadout customization with [BLREdit]().

You can find them in the User list on the left of the server under the "Server Status" role.

### Join by Address

- go to `Server Browser`
- at the `Custom Server` section enter the IP and Port of the game server
- click on `Connect`

After connecting to a server the client will start.

### Join by Browser <Badge text="unavailable" type="error"/>

::: danger Currently not available
The server browser is temporary unavailable due to a missing main server. Until the first release version you have to use the Server Address method!
:::

- go to `Server Browser`
- right-click on the server list and chose `Refresh`
- right-click on a server entry in the server list and chose `Connect->3.02`
<InlinePreview>
![UseServerBrowser](./UseServerBrowser.mp4)
</InlinePreview>


## :gun: Loadout Customization

Currently the in-game UI for loadout customization does not work but you can use external tools crafted by our awesome community. For loadout customization it is required to join our [Discord].

### Create Profile

1. Download [BLREdit](https://github.com/HALOMAXX/BLREdit/releases)
2. Enter your username in the top left field and select "Add Profile" to create a profile. **You must use this username on servers for your custom loadouts to apply!**
3. Configure your loadout
4. Hit the "Copy to clipboard" button in the top left to export the loadout to your clipboard.

### Upload Profile

1. Go to our [Discord] server
2. Select the server you want to connect to from the "Server Status" list on the right side
3. Send a DM to the server with this content: `register <clipoard content>`
4. The bot should reply with a success message

After connecting to the server you will have acces to your custom loadout.

::: warning Die once!
In order to apply your custom loadout you currently have to die once!
:::

## :book: FAQ

Before starting to seek help in our [Discord] make sure it isn't covered by this FAQ!

::: faq Client crashes when loading
Most likely you forgot to patch the client. If you did and it does still crash, seek help in our [Discord] or open an issue.
:::


::: faq "Can't connect to steam"
Your client failed to connect to the server. Either try a different server or report it to the server owner.
:::


## :children_crossing: Known Limitations

::: warning Loadout
1. Currently only three loadouts are supoported
1. The following items cannot be customized: Depot items, Emotes, Trophies, Voice Packs, Emblems
2. Customization is only available on the community servers provided in our [Discord]
::: 

::: warning Gamemodes
1. The Onslaught gamemode does not work
2. There are some unused/removed gamemodes visible in the gamemode selector, very few of these are fully functional, most have minimal functionality and only work on certain maps or are not playable at all
:::


[Discord]: https://discord.gg/wwj9unRvtN