# Improving the Wiki

Our wiki is generated using gitlab pages and vuepress.

## Report Issues

If you want to report an issue with the wiki you have to open an issue in
the [BLRevive/Wiki][1]), chose the template according to your issue from the list below 
and fill out the necessary parts.

| revision | improvement | bug |
| --- | --- | --- |
| <ul><li>false statements</li><li>broken links/images</li><li>outdated resources</li><li>misspells/typos</li></ul> | <ul><li>readability</li><li>new content</li></ul> | <ul><li>problems with resources</li><li>problems with rendering</li><li>problems with CI</li></ul>


## Local Repository + Markdown Editor <Badge>Full-Featured</Badge> <Badge type="warning">Advanced</Badge>

### prerequisites

- [git](https://git-scm.com/downloads)
- [node.js](https://nodejs.org/en/)
- markdown editor of your choice (eg [Typora](https://typora.io/))

### initial setup

Before you can start add/change content inside the wiki you first have to setup your local environment for that.

1. [fork] the [Wiki][1]
1. *recommended/optional*: [rename] your forked repository (to `blrevive-wiki` for eg)
1. clone your forked repository <InlinePreview>![CloneWiki](./CloneWiki.mp4)</InlinePreview>
1. install node dependencies (`npm install` or open `wiki_setup.bat`)

### edit

1. open preview with hot reload (`npm run dev` or open `wiki_dev.bat`)
    - wait until the window shows this URL: [http://localhost:8080/wiki](http://localhost:8080/wiki)
1. make your changes and review them directly in browser

::: tip make small changes and commit them
It is recommended to make small changes that can be described within a few words, rather make **lots** of then **huge** commits!
:::

### save changes (commit)

1. right-click inside the wiki folder and open Git GUI
1. click `Stage Changed`
1. enter a summary of your changes inside `Commit Message`
1. click `Commit` <InlinePreview>![Commit](./Commit.mp4)</InlinePreview>
1. click `Push` to upload the changes to your project

### merge changes 

Before we can merge your changes into the main branch (which will make them available in the official wiki)
you have to create a [merge request] on the `main` branch at the [Wiki][1] project.

::: tip make merge requests after pushing the first time
If you want other contributors to be able to see your progress and review your changes you should
consider making the merge request after pushing the first time and adding a `WIP: ` to the title.

This will prevent the merge request from being merged and enable the app review feature.
:::


### review merge requests

1. open the merge request in gitlab
1. scroll down to pipelines and click on `View app`
    <InlinePreview title="Example Merge Request">![ReviewApp](./app_review_mr.png)</InlinePreview>


## Gitlab Web IDE <Badge>Easy</Badge> <Badge type="warning">Limited</Badge>
::: warning limitations
Since Gitlab's markdown parser is not as featured as VuePress one the preview inside the
Gitlab Web IDE is not appropriate. If you want to have access to all features while writing
use the local repository + markdown editor approach!
:::

1. [fork] the [Wiki][1]
2. *recommended/optional*: [rename] your forked repository (to `blrevive-wiki` for eg)
1. open [web ide](https://docs.gitlab.com/ee/user/project/web_ide/) in your forked repository
1. after comitting your changes create a [merge request]



[1]: https://gitlab.com/blrevive/wiki
[fork]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork
[rename]: https://docs.gitlab.com/ee/user/project/settings/#renaming-a-repository
[merge request]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork