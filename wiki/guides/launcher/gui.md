# Launcher GUI
This is a reference for the launcher gui.

## Client

::: details Screenshot
![Client Tab](../../assets/images/launcher/client-tab.jpg)
:::

### Connect to Server

1. fill out `Username`, `Game Client`, `Server Address`, `Port`
2. *(optional)* add parameters for cli
3. click `Launch`


## Server Browser

::: details Screenshot
![Server Browser Tab](../../assets/images/launcher/server-browser-tab.jpg)
:::

### Server List

After refreshing, the server list will contain all known and active game servers for BLRevive.

#### refresh

- right-click into the list and click `refresh`

#### connect to server

- right-click on a server entry and click `connect` -> `[client]`

### Favorites

The favorites are a list of game servers you manage yourself.

## Server Utils

::: details Screenshot
![Server Utils Tab](../../assets/images/launcher/server-utils-tab.jpg)
:::

### Game Server

See [Hosting/Game Server](../hoster/game-server) for more information.

### Status Server

See [Hosting/Status Server](../hoster/status-server) for more information.

## Settings

::: details Screenshot
![Settings Tab](../../assets/images/launcher/settings-tab.jpg)
:::

### Game Clients

List of known BL:R clients.

#### add client

- Simply drag & drop the `[BLR install directory]/Binaries/Win32/FoxGame-win32-Shipping.exe` into the list.

*alternatively (if you're not on windows for eg)*:

- click on `Add`
- click on `Browse`
- open your  `[BLR install directory]/Binaries/Win32/FoxGame-win32-Shipping.exe`


#### patch client

- select the client in the list
- click on `Patch`

#### remove client

- select the client in the list
- click on `Remove`