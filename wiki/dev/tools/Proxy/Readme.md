# Proxy
A toolset for easy modification of the BL:R client.

::: warning naming confusion
The `Proxy.dll` currently used by the [Launcher] to provide statistics for game servers
is **not related** to this project and is named like that to be able to staticly inject
the DLL.
:::


![Proxy](./Proxy.svg)

## Module Loader <Badge>C++</Badge> <Badge>DLL</Badge>

The module loader is responsible for loading [Modules](#modules) at runtime and provide
a basic interface to the [Core SDK].

## Modules <Badge>C++</Badge>

A module (<Badge>Proxy-Module</Badge>) is a DLL which is loaded in the client by the [Module Loader]
to provide any kind of utility or modification.

The [Module Loader] will provide the [API] to all modules which gives access to several tools and libraries.

### examples

- [SDK Generator/Aggregator](/dev/tools/sdk-generator/#aggregator-c-proxy-module)

## API <Badge>C++</Badge> <Badge>LIB</Badge>

> TODO

[API]: #api
[Module Loader]: #module-loader 
[Core SDK]: #