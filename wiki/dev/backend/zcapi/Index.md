# ZCAPI

The ZCAPI provides a flexible server infrastructure which handles
everything that the *game server* does not.

## Introduction

The API is designed to be scalable and work among different machines/networks.
Thus, it provides a main class (`ZCAPI.Servers.ZCServerApp`) that manages a subset
of resources and provide some performance stats.

### API Resources

Resources are services that are registered to the [`ZCServerApp`] and provide a
particular functionality or dataset.

Almost any class inside the [`ZCAPI`] which handles dynamic, server sided data in some way can be
seen as a resource (and registered to the [`ZCServerApp`]).

### API Endpoints

Endpoints of the API are located inside the [`ZCAPI.Queries`] namespace.

### Encryption / Serializing

All packages send through the network are encrypted and serialized by the `ZCAPI.Clients.ZCConnectionBase` using `ZCAPI.XXTEA`.

Here's an example fiddle how to decrypt messages: [ZCAPI Encryption Example](https://dotnetfiddle.net/hxSBR4).

## ZCAPI vs PWAPI

As of the reunion of the developer studio (PWE to HSL) the PWAPI
got refactored into the newer ZCAPI. HSL decided to remove this API and the
`BLRCrashReporter.exe` which contained it from all post-parity clients.

Luckily, there was one strange published version ([v1.1.12](https://drive.google.com/file/d/1COLEPBFQ11FjxYo5VhEFkPdNLVap2cpb/view)) 
tightly coupled to Arc which contains the ZCAPI and post-parity patches aswell. This is where we got
the source code from.

### `ZCAPI` (~ZombieCureAPI) <Badge>recent</Badge>

This is the most recent version of the API and the version we will use for this project.

The endpoints of this API seem to match those of the v3.02 client and we were able to proof
that the client network encryption/serializing is the same as provided with ZCAPI.

- **Source Code: [ZCAPI]**

### `PWAPI` (~PerfectWorldAPI) <Badge type="warning">deprecated</Badge>

This is the legacy version of the API which was maintained by PWE. 
It differs alot from the ZCAPI and is way more chaotic.

- **Source Code: [PWAPI]**

[PWAPI]: https://gitlab.com/blrevive/reverse/blrcrashreporter
[ZCAPI]: https://gitlab.com/blrevive/reverse/blrcrashreporter/-/tree/1.12-Arc
[`ZCAPI`]: https://gitlab.com/blrevive/reverse/blrcrashreporter/-/tree/1.12-Arc/ZCAPI
[`ZCAPI.Queries`]: https://gitlab.com/blrevive/reverse/blrcrashreporter/-/tree/1.12-Arc/ZCAPI/Queries
[`ZCServerApp`]: https://gitlab.com/blrevive/reverse/blrcrashreporter/-/tree/1.12-Arc/ZCAPI/Servers/ZCServerApp.cs