# Launcher <Badge>C#</Badge> <Badge>.NET5</Badge> <Badge>AvaloniaUI</Badge> 

The launcher is a GUI for end users which provides easy access to several tools of BLRevive.

The main purposes are to abstract the patching process and ease the hosting and joining of game servers.

Since 0.4 the GUI is using [.NET5] & [AvaloniaUI] to achieve platform independency.


[AvaloniaUI]: https://github.com/AvaloniaUI/Avalonia
[.NET5]: https://dotnet.microsoft.com/download/dotnet/5.0