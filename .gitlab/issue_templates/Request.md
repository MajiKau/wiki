## Summary

> Summerize your request precisely.

## Use Case

> Explain the benefits of implementing your request.

## (optional) Proposal

> If you have any specific idea how to solve your request leave it here.